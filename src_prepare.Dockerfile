FROM registry.gitlab.com/src_prepare/gentoo-repoman-container:latest

ARG EMERGE_DEFAULT_OPTS=""
ARG FEATURES=""

COPY src_prepare-overlay.conf /etc/portage/repos.conf/

RUN <<EOF
mkdir -p /etc/portage/{repos.conf,package.accept_keywords}
echo "*/*::src_prepare-overlay **" > /etc/portage/package.accept_keywords/src_prepare-overlay.conf
emaint sync -r src_prepare-overlay
emerge app-eselect/eselect-repository app-portage/euscan-ng sys-apps/busybox
EOF
